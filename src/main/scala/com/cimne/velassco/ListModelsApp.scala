package com.cimne.velassco

import com.cimne.velassco.utils.ParseCLI
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by jorge on 28/09/16.
  */
object ListModelsApp {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName(this.getClass.getName)
    val sc = new SparkContext(conf)
    //sc.setLogLevel("INFO")

    var cmdopt = new ParseCLI(conf)
    cmdopt.setArgs(args)
    val catalog = new HCatalog(sc, cmdopt.getSuffix())

    println("The following models were found:")
    catalog.printModelIDs
  }

}
