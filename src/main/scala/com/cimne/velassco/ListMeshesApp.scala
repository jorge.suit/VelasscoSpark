package com.cimne.velassco

import com.cimne.velassco.utils.ParseCLI
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by jorge on 10/10/16.
  */
object ListMeshesApp {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName(this.getClass.getName)
    val sc = new SparkContext(conf)
    //sc.setLogLevel("INFO")

    var cmdopt = new ParseCLI(conf)
    cmdopt.setArgs(args)
    val catalog = new HCatalog(sc, cmdopt.getSuffix())

    println("The following models were found:")
    catalog.printModelIDs

    var keyModel = "".getBytes()
    var nameModel = ""
    if(cmdopt.getModelKey().length==0)
    {
      val indexModel = cmdopt.getModelIndex()
      keyModel = catalog.getModelKey(indexModel)
      nameModel = catalog.getModelName(indexModel)
    }
    else
    {
      keyModel = cmdopt.getModelKey()
      nameModel = Bytes.toString(keyModel)
    }

    catalog.printMeshes(keyModel)
  }
}
