package com.cimne.velassco

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by jorge on 14/09/16.
  */
object TestClosure {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("Test Clousure")
    val sc = new SparkContext(conf)

    val hi = "hola"
    sc.parallelize(Array(1, 2, 3, 4)).foreach(i => println(hi + " " + i))
    println("bye!")
  }
}

object doForeach {
  def fun(hi:String, sc: SparkContext, rdd: RDD[Int]): Unit = {
    rdd.foreach(i => println(hi + " " + i))
  }
}

object TestClosureApp extends App {
  val conf = new SparkConf().setAppName("Test Clousure")
  val sc = new SparkContext(conf)

  val rdd = sc.parallelize(Array(1, 2, 3, 4))
  doForeach.fun( "otro saludo", sc, rdd)

  val malo = "malo"
  rdd.foreach( i => println(malo + " " + i))

  println("bye!")
}