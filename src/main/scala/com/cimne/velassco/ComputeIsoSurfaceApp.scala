package com.cimne.velassco

import java.nio.{ByteBuffer,ByteOrder}
import java.io._

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem => HFileSystem, Path => HPath}
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import com.cimne.velassco.HCatalog._
import org.apache.hadoop.hbase.util.Bytes

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
//http://alvinalexander.com/scala/converting-java-collections-to-scala-list-map-array
import scala.collection.JavaConversions._

import FEM._
import utils._

/**
  * Created by jorge on 5/09/16.
  */

object IsoSurface {
  def test(resultPrefix:String, value:Double)(index: Int, iterInput: Iterator[(HKeyRaw,(HResultRaw,HResultRaw))]): Iterator[(HKeyRaw,(HResultRaw,HResultRaw))] = {
    //val indexResult = "000003"
    //val isoValue = 0.0
    println( "voy a pedir los bytes de indexResult que se supone nos ha llegado")
    val prefixResult = Array.concat(resultPrefix.getBytes, "vl_".getBytes())
    println( "identity for RDD_part=" + index + " and prefixResult = " + Bytes.toString(prefixResult))
    iterInput
  }

  def computeRddPartition(prefixColumn:String, value:Double, comp:Int)(index: Int, iterInput: Iterator[(HKeyRaw,(HResultRaw,HResultRaw))]): Iterator[(mutable.HashMap[Edge,Point],ListBuffer[TriangleEdge])] = {
    val mapEdges = new mutable.HashMap[Edge,Point]()
    val mapTriangles = new ListBuffer[TriangleEdge]()
    val preffix_m = "m".getBytes()
    val prefixResult = Array.concat(prefixColumn.getBytes, "vl_".getBytes)
    println( "computeRddPartition(" + Bytes.toString(prefixResult) + "," + value + ") for RDD_part=" + index)
    try {
      iterInput.foreach( row => {
        val columnsResult = row._2._1.getFamilyMap(Bytes.toBytes("R"))
        val columnsMesh = row._2._2.getFamilyMap(Bytes.toBytes("M"))
        columnsMesh.foreach( cell => {
          if (cell._1(0) == preffix_m(0)) {
            //println( "element = " + Bytes.toString(cell._1))
            //println( "prefixResult = " + Bytes.toString(prefixResult))
            genIsoTetra(cell._2, value, prefixResult, comp, columnsResult, columnsMesh, mapEdges, mapTriangles)
          }
        })
      })
    } catch {
      case e:Exception => {
        println("[computeRddPartition] -- unexpected exception: " + e.getMessage())
      }
    }
    Seq((mapEdges,mapTriangles)).iterator
  }
}

object ComputeIsoSurfaceApp {

  val ERR_RESULT_NOT_FOUND: Int = -1
  val ERR_INDEX_RANGE: Int = -2
  val ERR_EMPTY_OUTPUT:Int = -3
  val ERR_BAD_PARTITION:Int = -4

  var errorPath: String = ""

  def exitWithError(msg: String, errorCode: Int): Unit = {
    println("ERROR: " + msg);
    if(errorPath != "") {
      val pw = new PrintWriter(new File(errorPath))
      if (pw.checkError()) {
        println( "Unable to open '" + errorPath + "'")
      } else {
        pw.write(msg)
      }
      pw.close
    }
    sys.exit(errorCode)
  }

  def WriteOutput(outputPath: String,
                  edgesCut: Array[(Edge,Point)],
                  nodesId: mutable.Map[Edge,Long],
                  triangles:Array[TriangleEdge]): Unit = {
    val hadoopConfig = new Configuration()
    val hdfs = HFileSystem.get(hadoopConfig)
    val path = new HPath(outputPath)
    val os = hdfs.create(path, true)
    os.write(("NumberOfVertices: " + edgesCut.size + "\n").getBytes)
    os.write(("NumberOfFaces: " + triangles.size + "\n").getBytes)
    //val buffer = ByteBuffer.allocate((8+3*8)*edgesCut.size + (4+3*8)*triangles.size)
    val bufferNodes = ByteBuffer.allocate(8+3*8)
    bufferNodes.order(ByteOrder.LITTLE_ENDIAN)
    // write the nodes
    edgesCut.foreach( e => {
      bufferNodes.clear()
      bufferNodes.putLong(nodesId(e._1))
      bufferNodes.putDouble(e._2._1)
      bufferNodes.putDouble(e._2._2)
      bufferNodes.putDouble(e._2._3)
      os.write(bufferNodes.array)
    })
    // write the faces
    val bufferFaces = ByteBuffer.allocate(4+3*8)
    bufferFaces.order(ByteOrder.LITTLE_ENDIAN)
    val _3 = 3.toInt
    triangles.foreach( f => {
      bufferFaces.clear()
      val n1:Long = nodesId(f._1)
      val n2:Long = nodesId(f._2)
      val n3:Long = nodesId(f._3)
      bufferFaces.putInt(_3)
      bufferFaces.putLong(n1)
      bufferFaces.putLong(n2)
      bufferFaces.putLong(n3)
      os.write(bufferFaces.array)
    })
    os.close()
    hdfs.close()
  }

  def main(args: Array[String]): Unit = {
    def convertKeyToBytes(index: Int, iterInput: Iterator[(HKeyRaw, HResultRaw)]): Iterator[(RowKey, HResultRaw)] = {
      iterInput.map(it => (it._1.get(), it._2))
    }

    /*
    val msgStarting = "Starting spark process"
    println( msgStarting )
    TimeLog.timer(msgStarting)
    */

    val conf = new SparkConf().setAppName(this.getClass.getName)
    // http://stackoverflow.com/questions/35281538/spark-hbase-join-error-object-not-serializable-class-org-apache-hadoop-hbase-c
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[org.apache.hadoop.hbase.client.Result],classOf[String]) )
    conf.set("spark.driver.maxResultSize", "2g")

    val sc = new SparkContext(conf)


    //sc.setLogLevel("INFO")
    println("applicationId = " + sc.applicationId)

    //TimeLog.timer(msgStarting)

    var cmdopt = new ParseCLI(conf)
    cmdopt.setArgs(args)
    errorPath = cmdopt.getErrorPath()

    val catalog = new HCatalog(sc, cmdopt.getSuffix())

    println("The following models were found:")
    catalog.printModelIDs
    //catalog.getListOfModelNames.foreach(println)

    println("A modelkey is " + catalog.getModelKey(0).length + " length")

    var keyModel = "".getBytes()
    var nameModel = ""
    if(cmdopt.getModelKey().length==0)
    {
      val indexModel = cmdopt.getModelIndex()
      keyModel = catalog.getModelKey(indexModel)
      nameModel = catalog.getModelName(indexModel)
    }
    else
    {
      keyModel = cmdopt.getModelKey()
      nameModel = Bytes.toString(keyModel)
    }

    val analysis: String = cmdopt.getAnalysisName()

    var resultPrefix = ""
    var resultNComp:Int = 0
    if(cmdopt.getResultPrefix().size == 0)
    {
      val resultName = cmdopt.getResultName()
      val infoResult = catalog.getResultInfo(keyModel, analysis, cmdopt.getTimeStep(), resultName)
      if( infoResult("name")==resultName) {
        resultPrefix = infoResult("prefixColumn")
        resultNComp = infoResult("nComp").toInt
      } else {
        exitWithError("Result '"+ resultName +"' not found in metadata", ERR_RESULT_NOT_FOUND)
      }
    }
    else
    {
      resultPrefix = cmdopt.getResultPrefix
    }
    val componentResult = cmdopt.getResulComponent()
    if (componentResult >= resultNComp) {
      exitWithError("Requested component is out of range, must be in [0," + resultNComp + ")", ERR_INDEX_RANGE)
    }

    println("Requested result " + resultPrefix + "(" + componentResult + ") for Analysis = " + cmdopt.getAnalysisName() + " and timestep = " + cmdopt.getTimeStep())

    val numberOfPartitions = catalog.getNumberOfPartitions(keyModel)
    if (numberOfPartitions <= 0) {
      exitWithError("Invalid number of partitions "+numberOfPartitions+" for model " + keyModel, ERR_BAD_PARTITION)
    }
    val rddScanResult = catalog.getRddResult_Data(keyModel, cmdopt.getAnalysisName(), cmdopt.getTimeStep(), resultPrefix).coalesce(numberOfPartitions)
    val rddResult = rddScanResult.map(r => {
      (getHKeyMeshFromHKeyResult(r._1), r._2)
    })

    val meshId = cmdopt.getMeshId

    val rddScanMesh = catalog.getRddMesh_Data(keyModel, meshId).coalesce(numberOfPartitions)
    val rddResultAndMesh = rddResult.join(rddScanMesh)
    println( "JOIN has " + rddResultAndMesh.count() + " rows")
    //val rddIso = rddResultAndMesh.mapPartitionsWithIndex( (index,iter) => computeIsoSurface_RddPartition(index,iter, infoResult("index").asInstanceOf[Array[Byte]],cmdopt.getOptionDouble("--isovalue")))
    val isovalue = cmdopt.getOptionDouble("--isovalue")
    println( "isovalue = " + isovalue)
    //println( "indexResult = " + indexResult)
    val rddIso = rddResultAndMesh.mapPartitionsWithIndex(IsoSurface.computeRddPartition(resultPrefix, isovalue, componentResult))

    val traceRDDs = false

    val rddEdgesCut0 = rddIso.flatMap( _._1 )
    val countEdgesCut0 = rddEdgesCut0.count()
    if( countEdgesCut0 == 0 ) {
      exitWithError( "The output is empty", ERR_EMPTY_OUTPUT)
    } else {
      if (traceRDDs) {
        println("Isosurface has " + countEdgesCut0 + " edges-cut")
      }
    }
    val rddEdgesCut1 = rddEdgesCut0.reduceByKey( (x1,x2) => x1 )

    if (traceRDDs) {
      println("Isosurface has " + rddEdgesCut1.count() + " edges-cut after reduceByKey")
    }
    println( "Now renumbering edge-cut")
    val mapEdgeCut2Id = new mutable.HashMap[Edge,Long]()
    var edgesLocal = rddEdgesCut1.collect
    var idCurrent:Long = 1
    edgesLocal.foreach( it=>{
      mapEdgeCut2Id += (it._1 -> idCurrent)
      idCurrent += 1
    })
    println("Renumbering done");

    /*
    val sizesByPartition = rddEdgesCut1.mapPartitionsWithIndex((index,it) => Seq((index,it.size)).iterator).collectAsMap()
    val rddEdgesWithIndex = rddEdgesCut1.mapPartitionsWithIndex( (index,itPart) => {
      val result = new ArrayBuffer[(Edge,(Point,Long))]
      var i:Long = if( index == 0 ) 0 else sizesByPartition(index-1)
      itPart.map( it => {
        i += 1
        (it._1,(it._2,i))
      })
    })

    println( "Isosurface has " + rddEdgesWithIndex.count() + " edges-cut with index")
    */

    val rddTriangles = rddIso.flatMap( _._2 )
    if (traceRDDs) {
      println("Isosurface has " + rddTriangles.count() + " triangles")
      println("first triangle " + rddTriangles.first())
      println("first edge-cut " + rddEdgesCut1.first())
    }
    //println( "first edge-cut with index " + rddEdgesWithIndex.first() )
    //println( "first edge-cut with index " + mapEdgeCut2Id )

    if(cmdopt.getOutputPath!="")
      {
        //WriteOutput(cmdopt.getOutputPath, rddEdgesWithIndex.collectAsMap, rddTriangles.collect)
        WriteOutput(cmdopt.getOutputPath, edgesLocal, mapEdgeCut2Id, rddTriangles.collect)
        println("IsoSurface was written to '" + cmdopt.getOutputPath + "'")
      }

    //rddEdgesWithIndex.collect.foreach( println)

    //rddEdgesCut1.saveAsTextFile( "/tmp/kk.csv")
    /*
    println("Row keys for join")
    rddResultAndMesh.map(r => Bytes.toString(r._1.get())).collect().foreach(println(_))
    println( "Row keys for Result")
    rddResult.map(r => Bytes.toString(r._1.get())).collect().foreach(println(_))
    println( "Row keys for mesh")
    rddScanMesh.map(r => Bytes.toString(r._1.get())).collect().foreach(println(_))
    */
    println("BYE!")
    sc.stop()
  }
}

// spark-submit --class com.cimne.velassco.ComputeIsoSurfaceApp /home/jorge/IdeaProjects/VelasscoSpark/target/VelasscoSpark-0.1-SNAPSHOT.jar --model_idx 1 --suffix "_V4CIMNE" --analysis "geometry" --timestep 2 --result "PartitionId"
// spark-submit --class com.cimne.velassco.ComputeIsoSurfaceApp /home/jorge/IdeaProjects/VelasscoSpark/target/VelasscoSpark-0.1-SNAPSHOT.jar --model_idx 0 --suffix "_V4CIMNE" --analysis "Kratos" --timestep 21 --result "VELOCITY"
// spark-submit  --class com.cimne.velassco.ComputeIsoSurfaceApp /home/jorge/IdeaProjects/VelasscoSpark/target/VelasscoSpark-0.1-SNAPSHOT.jar --model_idx 1 --suffix "_V4CIMNE" --analysis "geometry" --timestep 2 --result "Vector function" --isovalue 0.5