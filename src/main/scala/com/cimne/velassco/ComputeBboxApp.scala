package com.cimne.velassco

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import com.cimne.velassco.HCatalog._
import org.apache.hadoop.hbase.CellUtil
//import org.apache.hadoop.hbase.spark.HBaseContext
//import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter
import org.apache.hadoop.hbase.client.Scan
//http://alvinalexander.com/scala/converting-java-collections-to-scala-list-map-array
import scala.collection.JavaConversions._


import Geometry._
import utils._

object BoundingBox {
  def compute(index:Int, iterPart: Iterator[(HKeyRaw, HResultRaw)]): Iterator[BBox] = {
    var inited:Boolean = false
    var BB:BBox = ((0.0,0.0), (0.0, 0.0), (0.0,0.0))
    iterPart.foreach( row => {
      row._2.rawCells().foreach(cell => {
        val value = CellUtil.cloneValue(cell)
        val pt:Point = (Bytes.toDouble(value, 0), Bytes.toDouble(value, 8), Bytes.toDouble(value, 16))
        BB = if( inited ) updBB0(BB, pt) else {
          inited = true
          ((pt._1, pt._1), (pt._2, pt._2), (pt._3, pt._3))
        }
      })
    })
    Seq(BB).iterator
  }
}

object ComputeBboxApp extends App {
  val conf = new SparkConf().setAppName(this.getClass.getName)
  val sc = new SparkContext(conf)

  var cmdopt = new ParseCLI(conf)
  cmdopt.setArgs(args)
  val indexModel = cmdopt.getModelIndex()

  val catalog = new HCatalog( sc, cmdopt.getSuffix() )

  println( "The following models were found:")
  catalog.printModelIDs
  //catalog.getListOfModelNames.foreach( println )

  println("Computing bounding box of the mesh")

  @transient val scan = buildScanNodes_Data( catalog.getModelKey(indexModel), -1)
  val hRdd = catalog.getRdd_Data( scan )
  // flatten the columns, result is a an RDD of <columna, coordenadas>
  //val rdd1 = hRdd.flatMap( it => it._2.getFamilyMap( Bytes.toBytes("M")))
  //val crd0 =rdd1.first()._2
  //val crd1 = (Bytes.toDouble(crd0, 0), Bytes.toDouble(crd0, 8), Bytes.toDouble(crd0, 16))

  // convert from byte[] to Point
  //val rddCoords = rdd1.map( it => (Bytes.toDouble(it._2, 0), Bytes.toDouble(it._2, 8), Bytes.toDouble(it._2, 16)) )

  //val pt0 = rddCoords.first;
  //val bb0 = ((pt0._1, pt0._1), (pt0._2, pt0._2), (pt0._3, pt0._3));
  //val res = rddCoords.aggregate(bb0)( (bb, pt) => updBB0(bb, pt), (bb1,bb2) => updBB1(bb1,bb2) )

  val rddBB = hRdd.mapPartitionsWithIndex( BoundingBox.compute)
  val res = rddBB.reduce( (b1,b2) => updBB1(b1,b2))
  println( "The bounding box is = " + res.toString() )
  println( "Bye!")

}


/**
 * Hello world!
 *
 */

/*
object ComputeBboxApp {
  def main( args: Array[String] ): Unit =
  {
    println( "Running ComputeBboxApp" )

    val conf = new SparkConf().setAppName("Compute Bounding Box")
    val sc = new SparkContext(conf)

    // if not declared as @transient then spark complains with:
    // org.apache.spark.SparkException: Task not serializable
    // ...
    // Caused by: java.io.NotSerializableException: org.apache.hadoop.conf.Configuration
    @transient val hconf = HBaseConfiguration.create()
    val hbaseContext = new HBaseContext(sc, hconf)

    // idem as org.apache.hadoop.conf.Configuration
    @transient val scan = new Scan()

    val tableModels = "VELaSSCo_Models_V4CIMNE";
    val tableData = "Simulations_Data_V4CIMNE";
    val rddModels = hbaseContext.hbaseRDD(TableName.valueOf(tableModels), scan);

    val allKeys = rddModels.map( x => x._1.get() ).collect();
    println( tableModels + " has " + allKeys.length + " models ..." );

    allKeys.foreach( x => println(Bytes.toString(x)) );

    val keyModelIndex = 1;

    scan.setRowPrefixFilter( allKeys(keyModelIndex) );

    scan.addFamily( Bytes.toBytes("M" ) );
    val f = new ColumnPrefixFilter( Bytes.toBytes("c"));
    scan.setFilter(f);
    scan.setCaching(100);

    val getRdd = hbaseContext.hbaseRDD(TableName.valueOf(tableData), scan)

    // flatten the columns, result is a an RDD of <columna, coordenadas>
    val rdd1 = getRdd.flatMap( it => it._2.getFamilyMap( Bytes.toBytes("M")))
    //val crd0 =rdd1.first()._2
    //val crd1 = (Bytes.toDouble(crd0, 0), Bytes.toDouble(crd0, 8), Bytes.toDouble(crd0, 16))

    // convert from byte[] to Point
    val rddCoords = rdd1.map( it => (Bytes.toDouble(it._2, 0), Bytes.toDouble(it._2, 8), Bytes.toDouble(it._2, 16)) )

    val pt0 = rddCoords.first;
    val bb0 = ((pt0._1, pt0._1), (pt0._2, pt0._2), (pt0._3, pt0._3));
    val res = rddCoords.aggregate(bb0)( (bb, pt) => updBB0(bb, pt), (bb1,bb2) => updBB1(bb1,bb2) )
    println( "The bounding box is = " + res.toString() )
    println( "Bye!")

  }

}

*/

// spark-submit --master yarn  --class com.cimne.velassco.ComputeBboxApp /home/jorge/IdeaProjects/VelasscoSpark/target/VelasscoSpark-0.1-SNAPSHOT.jar 0