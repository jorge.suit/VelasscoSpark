package com.cimne.velassco

import com.cimne.velassco.HCatalog._
import com.cimne.velassco.utils.ParseCLI
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.hadoop.hbase.util.Bytes

import scala.collection.JavaConversions._

/**
  * Created by jorge on 20/07/16.
  */
object TestHCatalog extends App {

  def countItems( index: Int, iterInput: Iterator[(HCatalog.HKeyRaw,HResultRaw)]): Iterator[(Int,Int)] = {
    println( "Counting partition " + index)
    iterInput.map( it => (index, it._2.getFamilyMap(Bytes.toBytes("M")).size())  )
  }

  val conf = new SparkConf().setAppName("Compute Bounding Box")
  val sc = new SparkContext(conf)

  //sc.setCheckpointDir("/tmp/spark/")

  //println("-------------Attach debugger now!--------------")
  //Thread.sleep(8000)

  var cmdopt = new ParseCLI(conf)
  cmdopt.setArgs(args)
  val indexModel = cmdopt.getModelIndex()

  val catalog = new HCatalog( sc )

  println( "The following models were found:")
  catalog.getListOfModelNames.foreach( println )

  /*
  println( "Counting number of elements for partition 1 of model " + catalog.getModelName(indexModel) )
  val rddRow : HBaseRDD = catalog.getRddElements_Data( catalog.getRowKey(indexModel), 1 );
  val rddElems = rddRow.flatMap( it => it._2.getFamilyMap( Bytes.toBytes("M")));
  println( "has " + rddElems.count + " elements" )
  println( "Counting number of elements by RDD partitions" )
  val rddAll = catalog.getRddElements_Data( catalog.getRowKey(indexModel), -1 )
  val rddCountByPartition = rddAll.mapPartitionsWithIndex( countItems )
  val sizesByPartition = rddCountByPartition.collect()
  println( "sizeByPartition has " + sizesByPartition.size + " elements")
  sizesByPartition.foreach(println)
  */
  catalog.getResultInfo( catalog.getModelKey(indexModel), cmdopt.getAnalysisName(), cmdopt.getTimeStep(), cmdopt.getResultName())
  println("BYE!")
}

// to debug remotely use this option:
// --driver-java-options -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005
