package com.cimne.velassco

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import com.cimne.velassco.HCatalog._
import org.apache.hadoop.hbase.CellUtil
import org.apache.hadoop.hbase.util.Bytes
//http://alvinalexander.com/scala/converting-java-collections-to-scala-list-map-array
import scala.collection.JavaConversions._

import FEM._
import utils._

object ComputeBoundaryApp extends App {

  //def computeRowBoundary
  def computeRddPartitionBoundary( index: Int, iterInput: Iterator[(HCatalog.HKeyRaw,HResultRaw)]): Iterator[(Triangle,Triangle)] = {
    println( "Computing local boundary for RDD_part=" + index)

    var mapFaces = new scala.collection.mutable.HashMap[Triangle,Triangle]()
    //var nRows = 0
    iterInput.foreach( row => {
      val msgRow = "Processing row " + Bytes.toString(row._1.get())
      println( msgRow )
      //TimeLog.timer(msgRow)
      //nRows += 1

      val columns = row._2.getFamilyMap(((Bytes.toBytes("M"))))
      columns.foreach( cell => {
        //insertBoundaryFacesFromTetra(cell._2, mapFaces)

        val faces = getTriangleSeqFromTetra( cell._2 )
        /*
        val faces = if(cell._2.length >= 32) {
          val n1 = Bytes.toLong(cell._2, 0)
          val n2 = Bytes.toLong(cell._2, 8)
          val n3 = Bytes.toLong(cell._2, 16)
          val n4 = Bytes.toLong(cell._2, 24)
          Seq((n1, n2, n3), (n1, n4, n2),(n2, n4, n3), (n1, n3, n4))
        } else Seq()*/
        faces.foreach(f => {
          val key = getKeyFromTriangle(f)
          if( mapFaces.contains(key) ) mapFaces.remove(key)
          else mapFaces(key) = f
        })
        if (faces.length == 0) {
          println( "at column " + Bytes.toString(cell._1))
        }
      })
      /*
      row._2.rawCells().foreach(cell => {
        val faces = getTriangleSeqFromTetra(CellUtil.cloneValue(cell))
        faces.foreach(f => {
          val key = getKeyFromTriangle(f)
          if (mapFaces.contains(key)) mapFaces.remove(key)
          else mapFaces(key) = f
        })
      })*/
      //TimeLog.timer(msgRow)
    })
    //println( "Computed local boundary for RDD_part=" + index + " which has " + nRows + " rows")
    mapFaces.iterator
  }

  val conf = new SparkConf().setAppName(this.getClass.getName)
  val sc = new SparkContext(conf)
  //sc.setLogLevel("INFO")
  println("applicationId = " + sc.applicationId)

  var cmdopt = new ParseCLI(conf)
  cmdopt.setArgs(args)
  val catalog = new HCatalog(sc, cmdopt.getSuffix())

  if (cmdopt.getScannerTimeout > 0) {
    println( "catalog.setHBaseClientScannerTimeout(" + cmdopt.getScannerTimeout + ")")
    catalog.setHBaseClientScannerTimeout(cmdopt.getScannerTimeout)
  }
  println("The following models were found:")
  catalog.printModelIDs
  //catalog.getListOfModelNames.foreach(println)

  var keyModel = "".getBytes()
  var nameModel = ""
  if(cmdopt.getModelKey().length==0)
  {
    val indexModel = cmdopt.getModelIndex()
    keyModel = catalog.getModelKey(indexModel)
    nameModel = catalog.getModelName(indexModel)
  }
  else
  {
    keyModel = cmdopt.getModelKey()
    nameModel = Bytes.toString(keyModel)
  }

  println("keyModel = " + Bytes.toString(keyModel))
  println("nameModel =" + nameModel)
  println("Computing boundary of the mesh for model " + nameModel)

  //TimeLog.timer( "Computing boundary of the mesh for model " + nameModel + " and mesh " + cmdopt.getMeshId)

  //val byRowKey = cmdopt.getPartByRow()
  @transient val scan1 = buildScanElements_Data(keyModel, cmdopt.getMeshId)

  val rddScan = catalog.getRdd_Data( scan1 )
  val rddBoundaryLocal = rddScan.mapPartitionsWithIndex(computeRddPartitionBoundary)
  val rddFacesReduced = rddBoundaryLocal.reduceByKey( (f1,f2) => (0,0,0) )
  val rddBoundary = rddFacesReduced.filter( it => it._2 != (0,0,0) )

  /*
  val rddElems = rddScan.flatMap(it => it._2.getFamilyMap( Bytes.toBytes("M")));
  val rddFaces = rddElems.flatMap( it => getTriangleSeqFromTetra(it._2))
  val rddFacesReduced = rddFaces.reduceByKey( (f1,f2) => (0,0,0) )
  val rddBoundary = rddFacesReduced.filter( it => it._2 != (0,0,0) )
  */
  println("The boundary has " + rddBoundary.count + " faces");
  //TimeLog.timer("Computing boundary of the mesh for model " + nameModel)
  sc.stop()
}

// spark-submit --class com.cimne.velassco.ComputeBoundaryApp /home/jorge/IdeaProjects/VelasscoSpark/target/VelasscoSpark-0.1-SNAPSHOT.jar 0
